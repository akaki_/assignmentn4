package com.example.calculator

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var multiply = false
        var perc = false
        var divide = false
        var subtract = false
        var add = false
        var root1 = false
        var number1 = 0.0
        var number2 = 0.0

        fun click(string: String){
            display1.append(string)
        }

        num1.setOnClickListener(){
            click("1")
        }
        num2.setOnClickListener(){
            click("2")
        }
        num3.setOnClickListener(){
            click("3")
        }
        num4.setOnClickListener(){
            click("4")
        }
        num5.setOnClickListener(){
            click("5")
        }
        num6.setOnClickListener(){
            click("6")
        }
        num7.setOnClickListener(){
            click("7")
        }
        num8.setOnClickListener(){
            click("8")
        }
        num9.setOnClickListener(){
            click("9")
        }
        num0.setOnClickListener(){
            click("0")
        }
        mult.setOnClickListener(){
            if (display1.text.isNotEmpty()){
            val text = display1.text.toString()
            number1 = text.toDouble()
            display1.text= ""
            multiply = true}
        }
        sub.setOnClickListener(){
            if (display1.text.isNotEmpty()){
            val text = display1.text.toString()
            number1 = text.toDouble()
            display1.text= ""
            subtract = true}
        }
        plus.setOnClickListener(){
            if (display1.text.isNotEmpty()){
            val text = display1.text.toString()
            number1 = text.toDouble()
            display1.text= ""
            add = true}
        }
        dvd.setOnClickListener(){
            if (display1.text.isNotEmpty()){
                val text = display1.text.toString()
                number1 = text.toDouble()
                display1.text= ""
                divide = true
            }
        }
        dot.setOnClickListener(){
            click(".")
        }
        C.setOnClickListener(){
            display1.text = ""
        }
        percent.setOnClickListener(){
            if (display1.text.isNotEmpty()){
                val text = display1.text.toString()
                number1 = text.toDouble()
                display1.text= ""
                perc = true
            }
        }
        plus_minus.setOnClickListener(){
            if (display1.text.isEmpty()){
            val text = display1.text
            display1.text = "-"
            display1.append(text)}
        }

        eql.setOnClickListener(){
            when {
                multiply -> {
                    val text = display1.text.toString()
                    number2 = text.toDouble()
                    val res = (number1*number2).toDouble()
                    display1.text = res.toString()
                    multiply = false
                }
                divide -> {
                    val text = display1.text.toString()
                    number2 = text.toDouble()
                    val res = (number1/number2).toDouble()
                    display1.text = res.toString()
                    divide = false
                }
                add -> {
                    val text = display1.text.toString()
                    number2 = text.toDouble()
                    val res = (number1+number2).toDouble()
                    display1.text = res.toString()
                    add = false
                }
                subtract -> {
                    val text = display1.text.toString()
                    number2 = text.toDouble()
                    val res = (number1-number2).toDouble()
                    display1.text = res.toString()
                    subtract = false
                }
                perc -> {
                    val text = display1.text.toString()
                    number2 = text.toDouble()
                    val res = (number1*number2/100).toDouble()
                    display1.text = res.toString()
                    perc = false
                }
            }
        }

    }
}